from django.contrib import admin
from blogs.models import PostCategory, Post, RelatedPost, PostComment
from django.utils.html import format_html
from django.forms import CheckboxSelectMultiple


# Register your models here.


@admin.register(PostCategory)
class PostCategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'created_at', 'updated_at']
    empty_value_display = '-empty-'


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['category', 'title', 'feature_image', 'slug']
    empty_value_display = '-empty-'

    list_filter = ('category', )

    def show_content(self, obj):
        return format_html(obj.content)


@admin.register(RelatedPost)
class RelatedPostAdmin(admin.ModelAdmin):
    list_display = ['post', 'get_related', 'created_at', 'updated_at']
    empty_value_display = '-empty-'

    filter_horizontal = ('related',)

    def get_related(self, obj):
        return ", ".join([r.title for r in obj.related.all()])


@admin.register(PostComment)
class PostCommentAdmin(admin.ModelAdmin):
    list_display = ['post', 'name', 'email', 'website', 'comment', 'is_approved', 'created_at']
