from django.urls import path
from django.conf.urls import url

from blogs import views

app_name = 'blogs'
urlpatterns = [
    path('', views.blog_page, name="blog_page"),
    path('<year>/<month>', views.blog_by_year_month_page, name="blog_by_year_month_page"),
    path('post-comment/', views.post_comment_view, name='post_comment'),
    path('unset/', views.unset, name="unset"),
    path('<slug:slug>/', views.blog_post, name="blog_post"),
    path('category/<int:id>/', views.blog_category, name="blog_category"),
]
