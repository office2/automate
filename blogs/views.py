from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
import simplejson as json
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.views.generic import View
from django.db.models import Count, Q

# Create your views here.
from blogs.forms import PostCommentForm
from blogs.models import Post, PostCategory, RelatedPost


def blog_page(request):
    category_name = request.GET.get('category', None)
    search = request.GET.get('search', None)
    if category_name:
        post_all = Post.objects.filter(category__name=category_name)
    elif search:
        post_all = Post.objects.filter(Q(title__icontains=search) | Q(content__icontains=search))
    else:
        post_all = Post.objects.all()
    category_all = PostCategory.objects.all()
    category_post_count = [{'name': category.name, 'count': Post.objects.filter(category__id=category.id).count()} for
                           category in
                           category_all]
    paginator = Paginator(post_all, 5)
    page = request.GET.get('page')
    try:
        post_data = paginator.page(page)
    except PageNotAnInteger as ex1:
        print("PAGE NOT INTEGER: ", ex1)
        post_data = paginator.page(1)
    except EmptyPage as ex2:
        print("PAGE EMPTY: ", ex2)
        post_data = paginator.page(paginator.num_pages)
    return render(request, 'blogs/blog_page.html',
                  {'post_data': post_data, 'category_data': category_post_count, 'year_range': range(2020, 2030),
                   'month_range': get_months()})


def blog_by_year_month_page(request, year, month):
    post_all = Post.objects.filter(
        created_at__year__gte=year,
        created_at__month__gte=month,
        created_at__year__lte=year,
        created_at__month__lte=month
    )
    category_all = PostCategory.objects.all()
    category_post_count = [{'name': category.name, 'count': Post.objects.filter(category__id=category.id).count()} for
                           category in
                           category_all]

    paginator = Paginator(post_all, 5)
    page = request.GET.get('page')
    try:
        post_data = paginator.page(page)
    except PageNotAnInteger as ex1:
        print("PAGE NOT INTEGER: ", ex1)
        post_data = paginator.page(1)
    except EmptyPage as ex2:
        print("PAGE EMPTY: ", ex2)
        post_data = paginator.page(paginator.num_pages)

    return render(request, 'blogs/blog_page.html',
                  {'post_data': post_data, 'category_data': category_post_count, 'year_range': range(2020, 2030),
                   'month_range': get_months()})


def blog_post(request, slug):
    post = get_object_or_404(Post, slug=slug)

    category_all = PostCategory.objects.all()
    category_post_count = [{'name': category.name, 'count': Post.objects.filter(category__id=category.id).count()} for
                           category in
                           category_all]
    return render(request, 'blogs/blog_post.html',
                  {'post': post,
                   'category_data': category_post_count,
                   'year_range': range(2020, 2030),
                   'month_range': get_months()})


def blog_category(request, id):
    post_data = Post.objects.filter(category=id)
    return render(request, 'blogs/blog_category.html', {'post_data': post_data})


def post_comment_view(request):
    if request.method == "POST":
        form = PostCommentForm(request.POST)
        post_id = request.POST.get('post')
        post = get_object_or_404(Post, pk=post_id)
        if form.is_valid():
            instance = form.save()
            request.session['comment'] = '1'
            return redirect('blogs:blog_post', slug=post.slug)
        return redirect('blogs:blog_post', slug=post.slug)


def unset(request):
    request.session['comment'] = None
    data = {"success": True, "message": "Session Unset"}
    return HttpResponse(json.dumps(data), content_type='application/json')


def get_monthsa():
    return ['January','February','March','April','May','June','July','August','September','October','November','December']
def get_months():
    return [
        {
            'name': 'January',
            'num': 1
        },
        {
            'name': 'February',
            'num': 2
        },
        {
            'name': 'March',
            'num': 3
        },
        {
            'name': 'April',
            'num': 4
        },
        {
            'name': 'May',
            'num': 5
        },
        {
            'name': 'June',
            'num': 6
        },
        {
            'name': 'July',
            'num': 7
        },
        {
            'name': 'August',
            'num': 8
        },
        {
            'name': 'September',
            'num': 9
        },
        {
            'name': 'October',
            'num': 10
        },
        {
            'name': 'November',
            'num': 11
        },
        {
            'name': 'December',
            'num': 12
        }
    ]
