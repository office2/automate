from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from eshop.models import App, default_app


# Create your models here.


class PostCategory(models.Model):
    name = models.CharField("Enter PostCategory Name", max_length=255, null=True, blank=True)
    description = models.TextField("Enter PostCategory Description", null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.name, self.description, self.created_at, self.updated_at)

    class Meta:
        verbose_name = "Post Category Management"
        verbose_name_plural = "Post Category Management"


class Post(models.Model):
    category = models.ForeignKey(PostCategory, on_delete=models.CASCADE, related_name="post_category_set")
    title = models.CharField("Enter Post Tittle", max_length=255, null=True, blank=True)
    slug = models.SlugField("Enter Post slug", max_length=255, null=True, blank=True)
    feature_image = models.ImageField("Select Feature Image", upload_to="blogs", null=True, blank=True)
    content = RichTextUploadingField(verbose_name="Enter Post Content", null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def get_feature_image_url(self):
        if self.feature_image and hasattr(self.feature_image, 'url'):
            return self.feature_image.url
        else:
            return "#"

    def __str__(self):
        return "{} - {}".format(self.category, self.title, self.feature_image, self.slug, self.content,
                                self.created_at, self.updated_at)

    class Meta:
        verbose_name = "Post Management"
        verbose_name_plural = "Post Management"


class PostComment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="post_comment_post_set")
    name = models.CharField(max_length=255, null=False)
    email = models.EmailField(null=False)
    website = models.CharField(max_length=255, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    is_approved = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.post, self.name, self.email, self.website, self.comment,
                                self.is_approved,
                                self.created_at, self.updated_at)

    class Meta:
        verbose_name = "Post Comment Management"
        verbose_name_plural = "Post Comment Management"


class RelatedPost(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="related_post_post")
    related = models.ManyToManyField(Post, related_name="related_post_related")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.post, self.related,
                                self.created_at, self.updated_at)

    class Meta:
        verbose_name = "Related Post Management"
        verbose_name_plural = "Related Post Management"
