from django import forms

from blogs.models import PostComment


class PostCommentForm(forms.ModelForm):
    class Meta:
        model = PostComment
        fields = ['post','name','email','website','comment']