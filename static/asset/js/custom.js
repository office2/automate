let my_slider = document.getElementById("myrange");
let output = document.getElementById("roi");
let roi_returns = document.getElementById("roi-returns");
if (my_slider != null) {
output.innerHTML = '<h1 style="margin-bottom: 2px;">' + my_slider.value + ' h</h1><p>timmar per vecka</p>' ;
}
function calculatePricing() {
	output.innerHTML = '<h1 style="margin-bottom: 2px;">' + my_slider.value + ' h</h1><p>timmar per vecka</p>' ;
	roi_returns.innerHTML = '<h1 style="margin-bottom: 2px;"> SEK ' + my_slider.value* 400;
	$('#showSavings').css('display','none');
  }
if (my_slider != null) {
  my_slider.oninput = function() {
    calculatePricing();
    }
} 

// var toggler = document.getElementsByClassName("caret");
// var i;

// for (i = 0; i < toggler.length; i++) {
//   toggler[i].addEventListener("click", function() {
//     this.parentElement.querySelector(".nested").classList.toggle("show");
//     this.classList.toggle("caret-down");
//   });
// }

$.fn.extend({
  treed: function (o) {
    var openedClass = "glyphicon-minus-sign";
    var closedClass = "glyphicon-plus-sign";

    if (typeof o != "undefined") {
      if (typeof o.openedClass != "undefined") {
        openedClass = o.openedClass;
      }
      if (typeof o.closedClass != "undefined") {
        closedClass = o.closedClass;
      }
    }

    //initialize each of the top levels
    var tree = $(this);
    tree.addClass("tree");
    tree
      .find("li")
      .has("ul")
      .each(function () {
        var branch = $(this); //li with children ul
        branch.prepend(
          "<i class='indicator glyphicon " + closedClass + "'></i>"
        );
        branch.addClass("branch");
        branch.on("click", function (e) {
          if (this == e.target) {
            var icon = $(this).children("i:first");
            icon.toggleClass(openedClass + " " + closedClass);
            $(this).children().children().toggle();
          }
        });
        branch.children().children().toggle();
      });
    //fire event from the dynamically added icon
    tree.find(".branch .indicator").each(function () {
      $(this).on("click", function () {
        $(this).closest("li").click();
      });
    });
    //fire event to open branch if the li contains an anchor instead of text
    tree.find(".branch>a").each(function () {
      $(this).on("click", function (e) {
        $(this).closest("li").click();
        e.preventDefault();
      });
    });
    //fire event to open branch if the li contains a button instead of text
    tree.find(".branch>button").each(function () {
      $(this).on("click", function (e) {
        $(this).closest("li").click();
        e.preventDefault();
      });
    });
  },
});

//Initialization of treeviews

$("#tree1").treed();

$("#tree2").treed({
  openedClass: "glyphicon-folder-open",
  closedClass: "glyphicon-folder-close",
});

$("#tree3").treed({
  openedClass: "glyphicon-chevron-right",
  closedClass: "glyphicon-chevron-down",
});
