"use strict";

var my_slider = document.getElementById("myrange");
var output = document.getElementById("roi");
var roi_returns = document.getElementById("roi-returns");

if (my_slider != null) {
  output.innerHTML = '<h1 style="margin-bottom: 2px;">' + my_slider.value + ' h</h1><p>timmar per vecka</p>';
}

function calculatePricing() {
  output.innerHTML = '<h1 style="margin-bottom: 2px;">' + my_slider.value + ' h</h1><p>timmar per vecka</p>';
  roi_returns.innerHTML = '<h1 style="margin-bottom: 2px;"> SEK ' + my_slider.value * 400;
  $('#showSavings').css('display', 'none');
}

if (my_slider != null) {
  my_slider.oninput = function () {
    calculatePricing();
  };
}

var toggler = document.getElementsByClassName("caret");
var i;

for (i = 0; i < toggler.length; i++) {
  toggler[i].addEventListener("click", function () {
    this.parentElement.querySelector(".nested").classList.toggle("show");
    this.classList.toggle("caret-down");
  });
}