$(window).on('load', function(){
	"use strict";


	/* ========================================================== */
	/*   Navigation Background Color      navbar-nav ml-auto                        */
	/* ========================================================== */

	$(window).on('scroll', function() {
		if($(this).scrollTop() > 450) {
			$('.navbar-fixed-top').addClass('opaque');
		} else {
			$('.navbar-fixed-top').removeClass('opaque');
		}
	});


	/* ========================================================== */
	/*   Hide Responsive Navigation On-Click                      */
	/* ========================================================== */

	  $(".navbar-nav li a").on('click', function(event) {
	    $(".navbar-collapse").collapse('hide');
	  });


	/* ========================================================== */
	/*   Navigation Color                                         */
	/* ========================================================== */

	$('#navbarCollapse').onePageNav({
		filter: ':not(.external)'
	});

	$('[data-toggle="tooltip"]').tooltip();

	/* ========================================================== */
	/*   SmoothScroll                                             */
	/* ========================================================== */

	 $(".navbar-nav li a, a.scrool").on('click', function(e) {
	 	var full_url = this.href;
	 	var parts = full_url.split("#");
	 	var trgt = parts[1];
	 	if (typeof trgt !== 'undefined') {
	 		var target_offset = $("#"+trgt).offset();
	 	    var target_top = target_offset.top;
	 	    $('html,body').animate({scrollTop:target_top -70}, 1000);
	 		    return false;
	 	}
	 	window.location.href = full_url;
	 });


	/* ========================================================== */
	/*   Newsletter                                               */
	/* ========================================================== */

	$('.newsletter-form').each( function(){
		var form = $(this);
		//form.validate();
		form.submit(function(e) {
			if (!e.isDefaultPrevented()) {
				jQuery.post(this.action,{
					'email':$('input[name="nf_email"]').val(),
				},function(data){
					form.fadeOut('fast', function() {
						$(this).siblings('p.newsletter_success_box').show();
					});
				});
				e.preventDefault();
			}
		});
	});


	/* ========================================================== */
	/*   Register                                                 */
	/* ========================================================== */

	$('#register-form').each( function(){
		var form = $(this);
		//form.validate();
		form.submit(function(e) {
			if (!e.isDefaultPrevented()) {
				jQuery.post(this.action,{
					'names':$('input[name="register_names"]').val(),
					'phone':$('input[name="register_phone"]').val(),
					'email':$('input[name="register_email"]').val(),
					'ticket':$('select[name="register_ticket"]').val(),
				},function(data){
					form.fadeOut('fast', function() {
						$(this).siblings('p.register_success_box').show();
					});
				});
				e.preventDefault();
			}
		});
	})


	/* ========================================================== */
	/*   Contact                                                  */
	/* ========================================================== */
	$('#contact-form').each( function(){
		var form = $(this);
		//form.validate();
		form.submit(function(e) {
			if (!e.isDefaultPrevented()) {
				jQuery.post(this.action,{
					'names':$('input[name="contact_names"]').val(),
					'subject':$('input[name="contact_subject"]').val(),
					'email':$('input[name="contact_email"]').val(),
					'phone':$('input[name="contact_phone"]').val(),
					'message':$('textarea[name="contact_message"]').val(),
				},function(data){
					form.fadeOut('fast', function() {
						$(this).siblings('p').show();
					});
				});
				e.preventDefault();
			}
		});
	})
});

var slider = document.getElementById("calculate-roi");
var output = document.getElementById("roi");
var roi_returns = document.getElementById("roi-returns");
if (slider != null) {
output.innerHTML = '<h1 style="margin-bottom: 2px;">' + slider.value + ' h</h1><p>timmar per vecka</p>' ;
}

function calculatePricing() {
	output.innerHTML = '<h1 style="margin-bottom: 2px;">' + slider.value + ' h</h1><p>timmar per vecka</p>' ;
	roi_returns.innerHTML = '<h1 style="margin-bottom: 2px;"> SEK ' + slider.value* 400;
	$('#showSavings').css('display','none');
  }
if (slider != null) {
    slider.oninput = function() {
    calculatePricing();
    }
}



function togglePricing() {
	if($('#services').css('display') == 'none') {
		$('#services').css('display','block');
	}
	else {
		$('#services').css('display','none');
	}
}

	/* ========================================================== */
	/*   Popup-Gallery                                            */
	/* ========================================================== */
	$('.popup-gallery').find('a.popup1').magnificPopup({
		type: 'image',
		gallery: {
		  enabled:true
		}
	});

	$('.popup-gallery').find('a.popup2').magnificPopup({
		type: 'image',
		gallery: {
		  enabled:true
		}
	});

	$('.popup-gallery').find('a.popup3').magnificPopup({
		type: 'image',
		gallery: {
		  enabled:true
		}
	});

	$('.popup-gallery').find('a.popup4').magnificPopup({
		type: 'iframe',
		gallery: {
		  enabled:false
		}
	});
