from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from eshop.models import App, default_app


class FaqCategory(models.Model):
    category_name = models.CharField("Category Name", max_length=255, null=False)
    description = models.TextField("Category Description", null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.category_name, self.description, self.created_at, self.updated_at)

    class Meta:
        verbose_name = 'Faq Category Management'
        verbose_name_plural = 'Faq Category Management'


# Create your models here.
class Faq(models.Model):
    category = models.ForeignKey(FaqCategory, on_delete=models.CASCADE, related_name="faq_category_set",
                                 null=True, blank=True)
    question = models.TextField("Enter Question", max_length=255, null=True, blank=True)
    answer = RichTextUploadingField(verbose_name="Enter Answer", null=True, blank=True)

    def __str__(self):
        return "{} - {}".format(self.category, self.question, self.answer)

    class Meta:
        verbose_name = "Faq Management"
        verbose_name_plural = "Faq Management"

