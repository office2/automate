from django.db.models import Q
from django.shortcuts import render
from faqs.models import Faq, FaqCategory
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


# Create your views here.


def faq(request):
    search_text = request.GET.get('search')
    if search_text is not None:
        faq_search = Faq.objects.filter(Q(category__category_name__icontains=search_text) |
                                        Q(category__description__icontains=search_text) |
                                     Q(question__icontains=search_text) | Q(answer__icontains=search_text))\
            .values_list('category_id', flat=True)
        faq_all = FaqCategory.objects.filter(pk__in=faq_search).order_by('created_at')
    else:
        search_text = ''
        faq_all = FaqCategory.objects.all().order_by('created_at')
    paginator = Paginator(faq_all, 5)
    page = request.GET.get('page')
    try:
        faqs = paginator.page(page)
    except PageNotAnInteger:
        faqs = paginator.page(1)
    except EmptyPage:
        faqs = paginator.page(paginator.num_pages)
    return render(request, 'faqs/faq.html', {'faqs': faqs, 'searchtext': search_text})


def faq_new(request):
    return render(request, 'faqs/faq_new.html')