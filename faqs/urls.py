from django.urls import path

from faqs import views

app_name = 'faqs'
urlpatterns = [
    path('', views.faq, name="faq"),
    path('new/', views.faq_new, name="faq_new"),
]
