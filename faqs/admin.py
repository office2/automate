from django.contrib import admin
from django.utils.html import format_html

from faqs.models import Faq, FaqCategory


@admin.register(FaqCategory)
class FaqCategoryAdmin(admin.ModelAdmin):
    list_display = ['category_name', 'description', 'created_at', 'updated_at', 'faq_count']

    def faq_count(self, obj):
        count = Faq.objects.filter(category=obj.id).count()
        return count


@admin.register(Faq)
class FaqAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
    list_display = ['category', 'question', 'ans_html']
    list_filter = ('category', )

    def ans_html(self, obj):
        return format_html(obj.answer)
