from django.contrib import admin
from products.models import ImpType, PayType, AccountingType, Product,RecommendedType,ProductRecommendation
# Register your models here.

@admin.register(ImpType)
class ImpTypeAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
    list_display = ['type_name', 'description', 'created_at', 'updated_at']


@admin.register(AccountingType)
class AccountingTypeAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
    list_display = ['type_name', 'description', 'created_at', 'updated_at']


@admin.register(PayType)
class PayTypeAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
    list_display =['type_name', 'description', 'created_at', 'updated_at']


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
    list_display =['product_name', 'product_image', 'description', 'import_type', 'accounting_type', 'payment_type', 'created_at', 'updated_at']


@admin.register(RecommendedType)
class RecommendedTypeAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
    list_display =['type_name', 'description', 'import_type', 'accounting_type', 'payment_type', 'created_at', 'updated_at']


@admin.register(ProductRecommendation)
class ProductRecommendationAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
    list_display =['product', 'recommended_type', 'created_at', 'updated_at']

