from django.db import models

# Create your models here.

class ImpType(models.Model):
    type_name = models.CharField("Enter Import Type Name", max_length=255, null=True, blank=True)
    description = models.TextField("Enter Import Type description", null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return "{} - {}".format(self.type_name, self.description, self.created_at, self.updated_at)

    class Meta:
        verbose_name = 'Import Type Management'
        verbose_name_plural = 'Import Type Management'


class AccountingType(models.Model):
    type_name = models.CharField("Enter Accounting Type Name", max_length=255, null=True, blank=True)
    description = models.TextField("Enter Accounting Type description", null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.type_name, self.description, self.created_at, self.updated_at)

    class Meta:
        verbose_name = 'Accounting Type Management'
        verbose_name_plural = 'Accounting Type Management'


class PayType(models.Model):
    type_name = models.CharField("Enter Payment Type Name", max_length=255, null=True, blank=True)
    description = models.TextField("Enter Payment Type description", null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.type_name, self.description, self.created_at, self.updated_at)

    class Meta:
        verbose_name = 'Payment Type Management'
        verbose_name_plural = 'Payment Type Management'


class Product(models.Model):
    product_name = models.CharField("Enter Product Name", max_length=255)
    product_image = models.ImageField("Enter Product Image", upload_to="product_image", null=True, blank=True)
    description = models.TextField("Enter Product description", null=True, blank=True)
    import_type = models.ForeignKey(ImpType, on_delete=models.CASCADE, related_name="product_import_type_set")
    accounting_type = models.ForeignKey(AccountingType, on_delete=models.CASCADE, related_name="product_accounting_type_set")
    payment_type = models.ForeignKey(PayType, on_delete=models.CASCADE, related_name="product_payment_type_set")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.product_name, self.product_image, self.description, self.import_type,self.accounting_type, self.payment_type, self.created_at, self.updated_at)

    class Meta:
        verbose_name = 'Product Type Management'
        verbose_name_plural = 'Product Type Management'

class RecommendedType(models.Model):
    type_name = models.CharField("Enter Recommended Type Name ", max_length=255, null=True, blank=True)
    description = models.TextField("Enter Recommended Type Description", null=True, blank=True)
    import_type = models.ForeignKey(ImpType, on_delete=models.CASCADE, related_name="recommendedtype_import_type_set")
    accounting_type = models.ForeignKey(AccountingType, on_delete=models.CASCADE,related_name="recommendedtype_accounting_type_set")
    payment_type = models.ForeignKey(PayType, on_delete=models.CASCADE, related_name="recommedndetype_payment_type_set")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.type_name, self.description, self.updated_at, self.created_at)

    class Meta:
        verbose_name = 'Recommended Type Management'
        verbose_name_plural = 'Recommended Type Management'

class ProductRecommendation(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="productrecommendation_product_set")
    recommended_type = models.ForeignKey(RecommendedType, on_delete=models.CASCADE, related_name="productrecommendation_recommended_type_set")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} - {}".format(self.product, self.recommended_type, self.created_at, self.updated_at)

    class Meta:
        verbose_name = 'ProductRecommendation Type Management'
        verbose_name_plural = 'ProductRecommendation Type Management'


