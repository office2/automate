from django.urls import path

from eshop import views
from shop import urls

app_name = 'eshop'
urlpatterns = [
    path('', views.landing_page, name="landing_page")
]
