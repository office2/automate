from django.contrib import admin

from .models import Load, App, Accounting, Payment, AppRecommendation, Addon, VolumeTier, BillingCycle
from .models import AboutUs, Feature, AppDescription, AppFAQ, AppCategory, ProductDetail, AppProductDetail, \
    AppProductImage, LandingPageSlug, CaseStudy


@admin.register(Load)
class LoadAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('name', 'phone_number', 'email', 'company_name', 'role', 'company_id')


@admin.register(App)
class AppAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('name', 'image', 'price', 'description', 'accounting', 'category')

    list_filter = (
        ('category', admin.RelatedOnlyFieldListFilter),
        ('accounting', admin.RelatedOnlyFieldListFilter),
    )

    def accounting(self):
        return ",".join([str(p) for p in self.accounting.all()]) 
    def category(self, obj):
        return obj.category.name

    category.admin_order_field = 'category__name'


@admin.register(ProductDetail)
class ProductDetailAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('name', 'short_description', 'long_description', 'bullet_list')


@admin.register(AppProductDetail)
class AppProductDetailAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('app_name', 'product_detail_name')

    list_filter = (
        ('app', admin.RelatedOnlyFieldListFilter),
        ('product_detail', admin.RelatedOnlyFieldListFilter),
    )

    def app_name(self, obj):
        return obj.app.name

    app_name.admin_order_field = 'app__name'

    def product_detail_name(self, obj):
        return obj.product_detail.name

    product_detail_name.admin_order_field = 'product_detail__name'


@admin.register(AppProductImage)
class AppProductImageAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('product_detail_name', 'image_tag')

    list_filter = (
        ('product_detail', admin.RelatedOnlyFieldListFilter),
    )

    def product_detail_name(self, obj):
        return obj.product_detail.name

    product_detail_name.admin_order_field = 'product_detail__name'


@admin.register(Accounting)
class AccountingAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('name', 'image', 'price')


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('name', 'image', 'price')


@admin.register(AppRecommendation)
class AppRecommendationAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('app_name', 'recommended_app_name')

    list_filter = (
        ('app', admin.RelatedOnlyFieldListFilter),
        ('recommended_app', admin.RelatedOnlyFieldListFilter),
    )

    def app_name(self, obj):
        return obj.app.name

    app_name.admin_order_field = 'app_name'

    def recommended_app_name(self, obj):
        return obj.recommended_app.name

    recommended_app_name.admin_order_field = 'recommended_app__name'


@admin.register(Addon)
class AddonAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('name', 'price')


@admin.register(VolumeTier)
class VolumeTierAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('name', 'volume', 'price_addon')


@admin.register(BillingCycle)
class BillingCycleAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('name', 'number_of_weeks_in_cycle', 'discount_percent')


@admin.register(AboutUs)
class AboutUsAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('description', 'image')


@admin.register(AppCategory)
class AppCategoryAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'


@admin.register(Feature)
class FeatureAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('title', 'description', 'icon', 'fa_icon_name', 'app_name')
    list_filter = (
        ('app', admin.RelatedOnlyFieldListFilter),
    )

    def app_name(self, obj):
        return obj.app.name

    app_name.admin_order_field = 'app__name'


@admin.register(AppDescription)
class AppDescriptionAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('title', 'description', 'app_name')
    list_filter = (
        ('app', admin.RelatedOnlyFieldListFilter),
    )

    def app_name(self, obj):
        return obj.app.name

    app_name.admin_order_field = 'app__name'


@admin.register(AppFAQ)
class AppFAQAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('title', 'description', 'app_name')
    list_filter = (
        ('app', admin.RelatedOnlyFieldListFilter),
    )

    def app_name(self, obj):
        return obj.app.name

    app_name.admin_order_field = 'app__name'


@admin.register(LandingPageSlug)
class LandingPageSlugAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('tag', 'app_name', 'accounting_name', 'payment_name', 'slug')

    list_filter = (
        ('app', admin.RelatedOnlyFieldListFilter),
        ('accounting', admin.RelatedOnlyFieldListFilter),
        ('payment', admin.RelatedOnlyFieldListFilter),
    )

    def app_name(self, obj):
        return obj.app.name

    app_name.admin_order_field = 'app__name'

    def accounting_name(self, obj):
        return obj.accounting.name

    accounting_name.admin_order_field = 'accounting__name'

    def payment_name(self, obj):
        return obj.payment.name

    payment_name.admin_order_field = 'payment__name'


@admin.register(CaseStudy)
class CaseStudyAdmin(admin.ModelAdmin):
    empty_value_display = 'N/A'
    list_display = ('industry_title', 'company_name', 'logo', 'request', 'process', 'result')
