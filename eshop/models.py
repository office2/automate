from django.db import models
from django.utils.html import mark_safe
from ckeditor_uploader.fields import RichTextUploadingField


# Create your models here.


class Addon(models.Model):
    name = models.CharField(max_length=255)
    price = models.DecimalField(default=0, decimal_places=2, max_digits=10)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Addon Management"
        verbose_name_plural = "Addon Management"


class BillingCycle(models.Model):
    name = models.CharField(max_length=255)
    number_of_weeks_in_cycle = models.IntegerField(default=0)
    discount_percent = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Billing Cycle Management"
        verbose_name_plural = "Billing Cycle Management"


class VolumeTier(models.Model):
    name = models.CharField(max_length=255)
    volume = models.IntegerField(default=0)
    price_addon = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Volume Tier Management"
        verbose_name_plural = "Volume Tier Management"


class Payment(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='payment', null=True)
    price = models.DecimalField(default=0, decimal_places=2, max_digits=10)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Payment Management"
        verbose_name_plural = "Payment Management"


def default_payment():
    return Payment.objects.get_or_create(name="N/A")[0].id


class Accounting(models.Model):
    name = models.CharField(max_length=255)
    payments = models.ManyToManyField(Payment)
    image = models.ImageField(upload_to='accounting', null=True)
    price = models.DecimalField(default=0, decimal_places=2, max_digits=10)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Accounting Management"
        verbose_name_plural = "Accounting Management"


def default_accounting():
    return Accounting.objects.get_or_create(name="N/A")[0].id


class AppCategory(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "App Category Management"
        verbose_name_plural = "App Category Management"


def default_category():
    return AppCategory.objects.get_or_create(name="Others")[0].id


class ProductDetail(models.Model):
    name = models.CharField(max_length=255)
    short_description = RichTextUploadingField(null=True, blank=True)
    long_description = RichTextUploadingField(null=True, blank=True)
    bullet_list = RichTextUploadingField(null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Product Detail Management"
        verbose_name_plural = "Product Detail Management"


def default_details():
    return ProductDetail.objects.get_or_create(name="N/A")[0].id


class App(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='apps', null=True)
    price = models.DecimalField(default=0, decimal_places=2, max_digits=10)
    description = models.TextField(default="")
    category = models.ForeignKey(AppCategory, null=False, default=default_category, on_delete=models.SET_DEFAULT)
    accounting = models.ManyToManyField(Accounting)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "App Management"
        verbose_name_plural = "App Management"


def default_app():
    return App.objects.get_or_create(name="N/A")[0].id


class AppProductDetail(models.Model):
    app = models.ForeignKey(App, on_delete=models.CASCADE, default=default_app)
    product_detail = models.ForeignKey(ProductDetail, on_delete=models.CASCADE, default=default_details)

    def __str__(self):
        return self.app.name + " - " + self.product_detail.name

    class Meta:
        verbose_name = "App Product Detail Management"
        verbose_name_plural = "App Product Detail Management"


class AppProductImage(models.Model):
    product_detail = models.ForeignKey(ProductDetail, related_name='product_images', default=default_details,
                                       on_delete=models.CASCADE)
    image = models.ImageField(upload_to="app_product_images", blank=True, null=True)

    def image_tag(self):
        return mark_safe('<img src="{}" width="150" height="150" />'.format(self.image.url))

    image_tag.short_description = 'product image'

    def __str__(self):
        return self.product_detail.name

    class Meta:
        verbose_name = "App Product Image Management"
        verbose_name_plural = "App Product Image Management"


class AppRecommendation(models.Model):
    app = models.ForeignKey(App, on_delete=models.CASCADE,
                            related_name='app',
                            default=default_app)  # landing page app

    recommended_app = models.ForeignKey(App, on_delete=models.CASCADE,
                                        related_name='recommended_app',
                                        default=default_app)  # which app it recommends

    class Meta:
        verbose_name = "App Recommendation Management"
        verbose_name_plural = "App Recommendation Management"


class Load(models.Model):
    name = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    company_name = models.CharField(max_length=255, default="")
    role = models.CharField(max_length=255, default="")
    company_id = models.CharField(max_length=255, default="")
    addons = models.ForeignKey(Addon, on_delete=models.SET_NULL, null=True)
    monthly_volume = models.ForeignKey(VolumeTier, on_delete=models.SET_NULL, null=True)
    billing_cycle = models.ForeignKey(BillingCycle, on_delete=models.SET_NULL, null=True)
    accounting = models.ForeignKey(Accounting, on_delete=models.SET_NULL, null=True)
    app_type = models.ForeignKey(App, on_delete=models.SET_NULL, null=True)
    payment = models.ForeignKey(Payment, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Load Management"
        verbose_name_plural = "Load Management"


class AboutUs(models.Model):
    description = models.TextField(default="")
    image = models.ImageField(upload_to='about_us', null=True)

    def __str__(self):
        return self.description

    class Meta:
        verbose_name = "About Us Management"
        verbose_name_plural = "About Us Management"


class AppFAQ(models.Model):
    app = models.ForeignKey(App, on_delete=models.CASCADE)
    title = models.CharField(max_length=255, default="")
    description = models.TextField(default="")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "App FAQ Management"
        verbose_name_plural = "App FAQ Management"


class Feature(models.Model):
    app = models.ForeignKey(App, on_delete=models.CASCADE, related_name="features_app_set", null=True, blank=True)
    title = models.CharField(max_length=255, default="")
    description = models.TextField(default="")
    icon = models.ImageField(upload_to='features', null=True, blank=True)
    fa_icon_name = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Feature Management"
        verbose_name_plural = "Feature Management"


class AppDescription(models.Model):
    title = models.CharField(max_length=255, default="")
    description = models.TextField(default="")
    app = models.ForeignKey(App, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "App Description Management"
        verbose_name_plural = "App Description Management"


class LandingPageSlug(models.Model):
    tag = models.CharField(max_length=255, default="")
    app = models.ForeignKey(App, on_delete=models.CASCADE)
    accounting = models.ForeignKey(Accounting, on_delete=models.CASCADE)
    payment = models.ForeignKey(Payment, null=True, default=None, on_delete=models.SET_DEFAULT)

    slug = models.SlugField(max_length=255, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = "/landing?" + "app=" + self.app.name + "&" + "accounting=" + self.accounting.name + "&" + "payment=" + self.payment.name if self.payment else ''
        super(LandingPageSlug, self).save(*args, **kwargs)  # Call the "real" save() method.

    def __str__(self):
        return self.tag

    class Meta:
        verbose_name = "Landing Page Generation Management"
        verbose_name_plural = "Landing Page Generation Management"


class CaseStudy(models.Model):
    industry_title = models.CharField(max_length=255)
    company_name = models.CharField(max_length=255)
    logo = models.ImageField(upload_to='case_studies', null=True, blank=True)
    request = RichTextUploadingField(null=True, blank=True)
    process = RichTextUploadingField(null=True, blank=True)
    result = RichTextUploadingField(null=True, blank=True)

    def __str__(self):
        return self.industry_title + " | " + self.company_name

    class Meta:
        verbose_name = "Case Study Management"
        verbose_name_plural = "Case Study Management"
