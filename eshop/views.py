from django.shortcuts import render, redirect
from django.views.generic import CreateView
from django.core.mail import send_mail
from .models import Load, App, Accounting, Payment, AppRecommendation, VolumeTier, BillingCycle, Feature, AboutUs, Load, \
    Addon, AppDescription, AppFAQ, AppProductDetail, ProductDetail, AppProductImage, AppCategory, CaseStudy, \
    LandingPageSlug
from .forms import LoadForm, SalesForm
from blogs.models import Post
from django.db.models import Q
from faqs.models import Faq
from django.http import Http404

from django.contrib import messages
import logging

# Create your views here.
logger = logging.getLogger(__name__)


def landing_page(request):
    form = LoadForm()
    salesform = SalesForm()
    response_message = 'Tack, ditt meddelande är mottaget. Vi återkopplar inom kort!'
    has_message = False

    recipient = 'kontakt@automatiseramera.se'

    request_app = None
    request_accounting = None
    request_payment = None

    if request.method == 'GET':
        request_app = request.GET.get('app', None)
        request_accounting = request.GET.get('accounting', None)
        request_payment = request.GET.get('payment', None)

    if request.method == 'POST':
        request_app = request.POST.get('app', None)
        request_accounting = request.POST.get('accounting', None)
        request_payment = request.POST.get('payment', None)

        submit_form = LoadForm(data=request.POST)
        if submit_form.is_valid():
            name = submit_form.cleaned_data['name']
            company_name = submit_form.cleaned_data['company_name']
            company_id = submit_form.cleaned_data['company_id']
            role = submit_form.cleaned_data['role']
            phone_number = submit_form.cleaned_data['phone_number']
            email = submit_form.cleaned_data['email']
            addons = submit_form.cleaned_data['addons']
            monthly_volume = submit_form.cleaned_data['monthly_volume']
            billing_cycle = submit_form.cleaned_data['billing_cycle']
            accounting = submit_form.cleaned_data['accounting']
            app_type = submit_form.cleaned_data['app_type']
            payment = submit_form.cleaned_data['payment']

            load = Load()
            load.name = name
            load.company_name = company_name
            load.company_id = company_id
            load.role = role
            load.phone_number = phone_number
            load.email = email
            load.addons = addons
            load.monthly_volume = monthly_volume
            load.billing_cycle = billing_cycle
            load.accounting = accounting
            load.app_type = app_type
            load.payment = payment

            load.save()
            has_message = True

            # email sending
            subject = "Contact me!"
            message = \
                """
                Name: {}\n
                Company Name: {}\n
                Company id: {}\n
                Role: {}\n
                Phone number: {}\n
                E-mail: {}\n
                """.format(name, company_name, company_id, role, phone_number, email)

            from_email = email
            print('Sending email')
            send_mail(subject, message, from_email, [recipient])
            print('Success')

    if request_app:
        default_slug = LandingPageSlug.objects.filter(app__name=request_app).first()
        if default_slug is None:
            raise Http404("Slug for App [{}] Not Found!".format(request_app))
        default_app = default_slug.app
    else:
        default_app = None

    if default_app is None:
        raise Http404("App [{}] Not Found!".format(request_app))

    all_slugs = LandingPageSlug.objects.all()

    # get available stuff for landing page dropdown lists
    available_apps = list(set([slug.app for slug in all_slugs]))
    available_accountings = list(set([slug.accounting for slug in all_slugs if slug.app.name == request_app]))
    available_payments = list(set([slug.payment for slug in all_slugs if slug.app.name == request_app]))

    # get defaults for accounting & payment
    if request_accounting:
        default_accounting = LandingPageSlug.objects.filter(app__name=request_app).first().accounting
    else:
        default_accounting = available_accountings[0] if available_accountings else None

    if default_accounting is None:
        raise Http404("Accounting for App [{}] Not Found!".format(request_app))

    if request_payment:
        default_payment = LandingPageSlug.objects.filter(app__name=request_app).first().payment
    else:
        default_payment = available_payments[0]

    app_description = AppDescription.objects.filter(app__name__contains=request_app).first()
    features = Feature.objects.filter(app__name__contains=request_app)
    recommendations = AppRecommendation.objects.filter(app__name__contains=request_app)
    faqs = AppFAQ.objects.filter(app__name__contains=request_app)

    return render(request,
                  'eshop/landing_page.html',
                  {
                      'form': form,
                      'salesform': salesform,
                      'apps': available_apps,
                      'addons': Addon.objects.all(),
                      'recommendations': recommendations if len(recommendations) else [],
                      'accountings': available_accountings,
                      'payments': available_payments,
                      'volume': VolumeTier.objects.all(),
                      'cycle': BillingCycle.objects.all(),
                      'about': AboutUs.objects.all(),
                      'features': features if len(features) else [],
                      'appDescription': app_description,
                      'faqs': faqs if len(faqs) else [],
                      'message': response_message,
                      'has_message': has_message,
                      'requested_app': default_app,
                      'default_accounting': default_accounting,
                      'default_payment': default_payment
                  }
                  )


def index_page(request):
    search = request.GET.get('search', None)
    if search:
        # posts = Post.objects.filter(Q(title__icontains=search) | Q(content__icontains=search))
        # faqs = Faq.objects.filter(Q(category__category_name__icontains=search) |
        #                           Q(category__description__icontains=search) |
        #                           Q(question__icontains=search) | Q(answer__icontains=search))
        # app_product_details = ProductDetail.objects.filter(
        #     Q(name__icontains=search) |
        #     Q(short_description__icontains=search) |
        #     Q(long_description__icontains=search) |
        #     Q(bullet_list__icontains=search))
        posts = Post.objects.filter(Q(title__icontains=search))
        faqs = Faq.objects.filter(Q(category__category_name__icontains=search) )
        app_product_details = ProductDetail.objects.filter(
            Q(name__icontains=search) )
        the_apps = App.objects.filter(
            Q(name__icontains=search) )
        accountings = Accounting.objects.filter(
            Q(name__icontains=search) )
        return render(request,
                      'eshop/search_results.html',
                      {
                          "blog_posts": posts,
                          "faqs": faqs,
                          "apps": app_product_details,
                          "the_apps": the_apps,
                          "accountings": accountings
                      }
                      )
    else:
        posts = Post.objects.all().order_by("-updated_at")[:3]
        return render(request,
                      'eshop/index.html',
                      {
                          "blog_posts": posts
                      }
                      )


def integrations(request):
    accountings = Accounting.objects.all()
    return render(request,
                  'eshop/integrations.html',
                  {
                      "accountings": accountings
                  }
                  )


def integrations_acounting(request, accounting):
    accounting_filtered = Accounting.objects.filter(name=accounting).first()
    if accounting_filtered:
        #apps = App.objects.filter(accounting__id=accounting_filtered.id)
        apps = App.objects.filter(accounting=accounting_filtered)
    else:
        apps = []

    app_categories_apps = {}
    app_categories = AppCategory.objects.all()
    for app_cat in app_categories:
        app_categories_apps[app_cat.name] = App.objects.filter(category__name__contains=app_cat.name).exclude(
            name__exact="N/A")
    return render(request,
                  'eshop/integrations_acounting.html',
                  {
                      "apps": apps,
                      'categories_apps': app_categories_apps,
                      'accounting': accounting_filtered
                  }
                  )


def case_studies(request):
    case_studies_fetched = CaseStudy.objects.all()
    return render(request,
                  'eshop/case_studies.html',
                  {
                      "case_studies": case_studies_fetched,
                  }
                  )


def partners(request):
    return render(request,
                  'eshop/partners.html', {}
                  )


def privacy(request):
    return render(request,
                  'eshop/privacy.html', {}
                  )


def service(request):
    return render(request,
                  'eshop/service.html', {}
                  )


def terms(request):
    return render(request,
                  'eshop/terms.html', {}
                  )


def product_details(request, app):
    app_product_details = AppProductDetail.objects.filter(app__name=app).first()
    app_fetched = {}
    product_detail_fetched = {}
    recommendations = []
    faqs = []
    features = []
    app_product_images = []

    if app_product_details:
        app_fetched = app_product_details.app
        product_detail_fetched = app_product_details.product_detail
        recommendations = AppRecommendation.objects.filter(app__name__contains=app_fetched.name)
        features = Feature.objects.filter(app__name__contains=app)
        faqs = AppFAQ.objects.filter(app__name__contains=app)
        app_product_images = AppProductImage.objects.filter(product_detail__name__contains=product_detail_fetched.name)

    app_categories_apps = {}
    app_categories = AppCategory.objects.all()
    for app_cat in app_categories:
        app_categories_apps[app_cat.name] = App.objects.filter(category__name__contains=app_cat.name).exclude(
            name__exact="N/A")

    return render(request,
                  'eshop/product_details.html',
                  {
                      "app": app_fetched,
                      "product_detail": product_detail_fetched,
                      'features': features,
                      'faqs': faqs,
                      'recommendations': recommendations,
                      'app_product_images': app_product_images,
                      'categories_apps': app_categories_apps
                  }
                  )
